from unittest.mock import patch, Mock

import pytest

from eveapi import sso
from eveapi.esi.errors import ESIPermissionRevoked


class TestAuthorize:
    @patch('eveapi.sso.re')
    def test_success(self, re):
        result = Mock()
        re.post.return_value = result
        result.status_code = 200
        result.json.return_value = 'result'

        assert 'result' == sso.authorize(12345)
        re.post.assert_called_once_with(
            'https://login.eveonline.com/oauth/token',
            json={'grant_type': 'authorization_code', 'code': 12345},
            headers={'Authorization': 'Basic Q0xJRU5UX0lEOkNMSUVOVF9TRUNSRVQ='},
            timeout=10000
        )
        result.json.assert_called_once()

    @patch('eveapi.sso.re')
    def test_failure(self, re):
        result = Mock()
        re.post.return_value = result
        result.status_code = 500

        with pytest.raises(Exception, match='Error authorizing'):
            sso.authorize(12345)


class TestVerify:
    @patch('eveapi.sso.re')
    def test_success(self, re):
        result = Mock()
        re.get.return_value = result
        result.status_code = 200
        result.json.return_value = 'result'

        assert 'result' == sso.verify('Q0xJRU5UX0lEOkNMSUVOVF9TRUNSRVQ', 'Basic')
        re.get.assert_called_once_with(
            'https://login.eveonline.com/oauth/verify',
            headers={'Authorization': 'Basic Q0xJRU5UX0lEOkNMSUVOVF9TRUNSRVQ'},
            timeout=10000
        )
        result.json.assert_called_once()

    @patch('eveapi.sso.re')
    def test_failure(self, re):
        result = Mock()
        re.get.return_value = result
        result.status_code = 500

        with pytest.raises(Exception, match='Error verifying character'):
            sso.verify('Q0xJRU5UX0lEOkNMSUVOVF9TRUNSRVQ', 'Basic')


class TestRefresh:
    @patch('eveapi.sso.re')
    def test_success(self, re):
        expected = {'token': 'token'}

        result = Mock()
        re.post.return_value = result
        result.status_code = 200
        result.json.return_value = expected

        assert expected == sso.refresh(1, 'Q0xJRU5UX0lEOkNMSUVOVF9TRUNSRVQ')
        re.post.assert_called_once_with(
            'https://login.eveonline.com/oauth/token',
            json={"grant_type": "refresh_token", "refresh_token": 'Q0xJRU5UX0lEOkNMSUVOVF9TRUNSRVQ'},
            headers={'Authorization': 'Basic Q0xJRU5UX0lEOkNMSUVOVF9TRUNSRVQ='},
            timeout=10000
        )
        result.json.assert_called_once()

    @patch('eveapi.sso.re')
    def test_revoked_failure(self, re):
        result = Mock()
        re.post.return_value = result
        result.json.return_value = {'error': 'invalid_token'}
        result.status_code = 403

        with pytest.raises(ESIPermissionRevoked):
            sso.refresh(1, 'Q0xJRU5UX0lEOkNMSUVOVF9TRUNSRVQ')

    @patch('eveapi.sso.re')
    def test_other_failure(self, re):
        result = Mock()
        re.post.return_value = result
        result.json.return_value = {'token': 'token'}
        result.status_code = 500

        with pytest.raises(Exception, match='Error refreshing token'):
            sso.refresh(1, 'Q0xJRU5UX0lEOkNMSUVOVF9TRUNSRVQ')
